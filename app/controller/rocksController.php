<?php

class rocksController {

	public function index() {
		require_once($_SERVER['DOCUMENT_ROOT'].'/app/model/categories.php');
		$categories = new Categories();
		$categories = $categories->getCategories();

		require_once($_SERVER['DOCUMENT_ROOT'].'/app/model/products.php');
		$products = new Products();
		$products = $products->getProducts();

		require_once($_SERVER['DOCUMENT_ROOT'].'/app/view/rocks/index.ttp');
	}
	
	public function product() {
		require_once($_SERVER['DOCUMENT_ROOT'].'/app/model/categories.php');
		$categories = new Categories();
		$categories = $categories->getCategories();

		require_once($_SERVER['DOCUMENT_ROOT'].'/app/model/products.php');
		$products = new Products();
		$product_id = $products->getCurrentProduct($_SERVER['REQUEST_URI']);
		$products = new Products();
		$product = $products->getProductById($product_id);

		require_once($_SERVER['DOCUMENT_ROOT'].'/app/view/rocks/product.ttp');
	}

	public function category() {
		require_once($_SERVER['DOCUMENT_ROOT'].'/app/model/categories.php');
		$categories = new Categories();
		$categories = $categories->getCategories();
		$category   = new Categories();
		$category_id = $category->getCurrentCategory($_SERVER['REQUEST_URI']);

		require_once($_SERVER['DOCUMENT_ROOT'].'/app/model/products.php');
		$products = new Products();
		$products = $products->getProductsByCategory($category_id);

		require_once($_SERVER['DOCUMENT_ROOT'].'/app/view/rocks/index.ttp');
	}
}