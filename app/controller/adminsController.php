<?php

class AdminsController {
	public function index() {
		if (isset($_COOKIE['user']) && $_COOKIE['user'] != '') {
			require_once($_SERVER['DOCUMENT_ROOT'].'/app/model/categories.php');
			$categories = new Categories();
			$categories = $categories->getCategories();

			require_once($_SERVER['DOCUMENT_ROOT'].'/app/model/products.php');
			$products = new Products();
			$products = $products->getProducts();

			require_once($_SERVER['DOCUMENT_ROOT'].'/app/view/admins/index.ttp');
		} else {
			header('Location: ./');
		}
	}

	public function createcategory() {
		if (isset($_COOKIE['user']) && $_COOKIE['user'] != '') {
			if (isset($_POST['name'])) {
				$image = (isset($_COOKIE['filename'])) ? $_COOKIE['filename'] : '';
				require_once($_SERVER['DOCUMENT_ROOT'].'/app/model/categories.php');
				$categories = new Categories();
				$categories->createCategory($_POST, $image);
				header('Location: ./');
			} else {
				setcookie('filename', '', time()-1);
				require_once($_SERVER['DOCUMENT_ROOT'].'/app/view/admins/createcategory.ttp');
			}

		} else {
			header('Location: ./');
		}
	}

	public function createproduct() {
		if (isset($_COOKIE['user']) && $_COOKIE['user'] != '') {
			if (isset($_POST['name'])) {
				$image = (isset($_COOKIE['filename'])) ? $_COOKIE['filename'] : '';
				require_once($_SERVER['DOCUMENT_ROOT'].'/app/model/products.php');
				$products = new Products();
				$products->createProduct($_POST, $image);
				header('Location: ./');
			} else {
				setcookie('filename', '', time()-1);
				require_once($_SERVER['DOCUMENT_ROOT'].'/app/model/categories.php');
				$categories = new Categories();
				$categories = $categories->getCategories();
				require_once($_SERVER['DOCUMENT_ROOT'].'/app/view/admins/createproduct.ttp');
			}
		} else {
			header('Location: ./');
		}
	}

	public function fileupload() {
    $ext = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
    $acceptable_extensions = ['jpg', 'jpeg', 'png', 'gif'];
    if (in_array($ext, $acceptable_extensions)) {
      $upload_folder = $_SERVER["DOCUMENT_ROOT"].'/webroot/img/upload_folder/';
      $filename = time().'.'.$ext;

      if (move_uploaded_file($_FILES['image']['tmp_name'], $upload_folder.$filename)) {
        $this->resizeImage($upload_folder.$filename, $upload_folder.'450_'.$filename, 450, 450);
        $this->resizeImage($upload_folder.$filename, $upload_folder.'250_'.$filename, 250, 250);
        $this->resizeImage($upload_folder.$filename, $upload_folder.'110_'.$filename, 110, 110);
        setcookie('filename', $filename, time()+86400);
      }
      echo $filename;
    } else {
      echo 'Unacceptable extension';
      return false;
    }
  }

  	private function resizeImage($file, $target, $targetWidth, $targetHeight) {
	    list($width, $height) = getimagesize($file);
	    $imgString = file_get_contents($file);
	    $image = imagecreatefromstring($imgString);

	    if ($width > $height) {
	      $y = 0;
	      $x = ($width - $height) / 2;
	      $smallestSide = $height;
	    } else {
	      $x = 0;
	      $y = ($height - $width) / 2;
	      $smallestSide = $width;
	    }

	    $thumb = imagecreatetruecolor($targetWidth, $targetHeight);
	    imagecopyresampled($thumb, $image, 0, 0, $x, $y, $targetWidth, $targetHeight, $smallestSide, $smallestSide);

	    switch ($_FILES['image']['type']) {
	      case 'image/jpeg':
	        imagejpeg($thumb, $target);
	        break;
	      case 'image/png':
	        imagepng($thumb, $target);
	        break;
	      case 'image/gif':
	        imagegif($thumb, $target);
	        break;
	      default:
	        exit;
	        break;
	    }

	    return true;
	}

	public function login() {
		if (isset($_POST['username']) && isset($_POST['password'])) {
			require_once($_SERVER['DOCUMENT_ROOT'].'/app/model/admins.php');
			$user_id = Admins::login($_POST['username'], $_POST['password']);
			if (!empty($user_id)) {
				setcookie('user', $user_id, time()+86400, "/");
				header('Location: ./');
			} else {
				return false;
			}
		} else {
			require_once($_SERVER['DOCUMENT_ROOT'].'/app/view/admins/login.ttp');
		}
	}

	public function logout() {
		setcookie('user', '', time()-1, "/");
		header('Location: ./');
	}

}