<?php
$router = new Router();

//addRoute($pattern, $controller, $action);
$router->addRoute("/^\/rocks\/(c\d+)$/i", "rocks", "category");
$router->addRoute("/^\/rocks\/(p\d+)$/i", "rocks", "product");
$router->addRoute("/^\/(admins)\/(login)$/i", "admins", "login");
$router->addRoute("/^\/(admins)\/(logout)$/i", "admins", "logout");
$router->addRoute("/^\/(admins)\/(createcategory)$/i", "admins", "createcategory");
$router->addRoute("/^\/(admins)\/(createproduct)$/i", "admins", "createproduct");
$router->addRoute("/^\/(admins)\/(fileupload)$/i", "admins", "fileupload");
$router->addRoute("/^\/(admins)$/i", "admins");

class ControllerFactory {
    public function createFromRouter(Router $router) {
        $result = $router->fetch($_SERVER['REQUEST_URI']);
        if (isset($result)) {
            $controller = key($result).'Controller';
            $action = array_shift($result);
            
            
            if (require_once($_SERVER['DOCUMENT_ROOT'].'/app/controller/'.$controller.'.php')) {
                $controller = new $controller();
                $controller->$action();
            } else {
                throw new Exception('In order to find new planets you must invent some telescopes first');
            }
        }
    }
}
$factory = new ControllerFactory();