<?php
class Router {
  private $routes = array();
  private $defaultController = array('rocks' => 'index');

  public function addRoute($pattern, $controller, $action = "index") {
      $this->routes[] = array(
          "pattern" => $pattern,
          "controller" => $controller,
          "action" => $action
      );
  }

  public function fetch($url) {

      foreach ($this->routes as $route) {
          preg_match($route['pattern'], $url, $matches);

          if ($matches) {
          		foreach (array_column($this->routes, 'pattern') as $key=>$pattern) {
          			preg_match($pattern, $matches[0], $route[$key]);
          		}  	
              	$controllerAction[$route['controller']] = $route['action']; 
              return $controllerAction;
          } 

      }
      return $this->defaultController;
  }
}