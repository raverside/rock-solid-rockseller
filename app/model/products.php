<?php 
class Products {
	public function getProducts() {
		$db = Db::getInstance();
		$products = $db->query("SELECT * FROM products");
		return $products->fetchAll();
	}
  public function getProductById($id) {
    $db = Db::getInstance();
    $products = $db->query("SELECT * FROM products WHERE `id`='".$id."' LIMIT 1");
    return $products->fetch();
  }
  public function getProductsByCategory($category_id) {
    $db = Db::getInstance();
    $products = $db->query("SELECT p.name, p.id, p.image, p.price, p.sale_price FROM product_category pa JOIN products p on p.id = pa.product_id JOIN categories c on c.id = pa.category_id WHERE c.id = '".$category_id."'");
    return $products->fetchAll();
  }
  public function getCurrentProduct($url) {
    $product_id = substr($url, strrpos($url, 'p') + 1);
    return $product_id;
  }
	public function createProduct($post, $image) {
      $db = Db::getInstance();
      $db->query("INSERT INTO products (`name`,`image`, `price`, `sale_price`) VALUES ('".$post['name']."','".$image."', '".$post['price']."', '".$post['sale_price']."')");
      $product_id = $db->lastInsertId();
      foreach ($post['category'] as $category) {
      	$db->query("INSERT INTO product_category (`category_id`, `product_id`) VALUES ('".$category."','".$product_id."')");
      }
    }
}