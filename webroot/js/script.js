$(document).ready(function(){
	$('.create').click(function(e){
		e.preventDefault();
		$.ajax({
			url: $(this).attr('href'),
			success: function(data) {
				$('.modal').fadeIn();
				$('.modal').html(data);
			},
		});
	});
	$(document).on('click', '.close-modal', function() {
		$('.modal').fadeOut();
	});
	$(document).on('change', '#upload-image', function() {
		var formData = new FormData();
		formData.append('image', $('#upload-image')[0].files[0]);

		$.ajax({
		       url : '/admins/fileupload',
		       type : 'POST',
		       data : formData,
		       processData: false,  // tell jQuery not to process the data
		       contentType: false,  // tell jQuery not to set contentType
		       success: function(data) {
		        	$('.image-upload').text('Image uploaded');
		        	$('.image-upload').attr('data-upload', data);
		        	$('.image-upload').addClass('uploaded');
		       },
		       error: function(data) {
		       		console.log(data);
		       }
		});
	});
});