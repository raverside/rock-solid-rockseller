<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once(dirname(__DIR__) . '/app/config/db.php');
require_once(dirname(__DIR__) . '/app/config/router.php'); 
require_once(dirname(__DIR__) . '/app/config/render.php'); 
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {  
	include_once(dirname(__DIR__) . '/app/view/layout/ajax.ttp');
  	exit;    
}
include_once(dirname(__DIR__) . '/app/view/layout/default.ttp');